pipeline {
  agent {
    docker {
      image 'localhost:5000/jenkins/ifs'
      args '-v /var/lib/build -v /var/lib/jenkins:/var/lib/jenkins -v /var/lib/jenkins/.ssh:/home/jenkins/.ssh'
    }
  }
  options {
    buildDiscarder(logRotator(numToKeepStr:'2'))
  }
  environment {
    SLACK_URL='https://hooks.slack.com/services/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'
  }
  post {
    always {
//      recordIssues enabledForFailure: true, aggregatingResults: true, tool: checkStyle(pattern: './checkstyle-result.xml', name: "CheckStyle Issues" )
      recordIssues enabledForFailure: true, tools: [checkStyle(name: 'Lint Violations', pattern: 'check*.xml')]
      publishHTML target: [
        allowMissing: true,
        alwaysLinkToLastBuild: false,
        keepAll: true,
        reportDir: 'coverage',
        reportFiles: 'index.html',
        reportName: 'Code Coverage Details'
      ]
      publishHTML target: [
        allowMissing: true,
        alwaysLinkToLastBuild: false,
        keepAll: false,
        reportDir: 'doc',
        reportFiles: 'index.html',
        reportName: 'API Documentation'
      ]
      cobertura failNoReports: false, coberturaReportFile: 'coverage/cobertura-coverage.xml'
      deleteDir()
    }
    failure {
      // TODO: if running on the 'official' Jenkins, send a message.  Otherwise not so much
      script {
        if ( "${GIT_BRANCH}"  == 'dev' ) {
          echo "Sending slack notification for failure in branch ${GIT_BRANCH}"
          // sh "curl -X POST -H 'Content-type: application/json' --data '{\"text\": \"FAILURE: ${JOB_NAME}, Build: ${BUILD_ID} at ${BUILD_URL}\"}' ${SLACK_URL}"
        }
      }
    }
  }
  stages {
    stage('configure') {
      steps {
        echo 'Configuring'
        echo "Starting ${JOB_NAME}, Build: ${BUILD_ID} at ${BUILD_URL}"
        sh 'npm install'
      }
    }
    stage('lint') {
      steps {
        echo 'running lint'
        sh 'npm run lint || exit 0'
      }
    }
    stage('docs') {
      steps {
        echo 'Documentation'
        sh 'npm run docs || exit 0'
      }
    }
    stage('Test') {
      steps {
        echo 'Testing'
        sh 'npm run test || exit 0'
      }
    }
    stage('Build') {
      steps {
        echo 'Building'
        echo 'npm run build || exit 0'
      }
    }
    stage('Publish') {
      steps {
        sh 'npm pack'
        archiveArtifacts artifacts: '*.tgz', fingerprint: true, onlyIfSuccessful: true
        sshagent(['ifsdev']) {
          sh 'scp -i ~/.ssh/ifsdev.pem ./*.tgz ubuntu@extdev.teamandtech.com:/home/tnt-ts-api/tnt-ts-api.tgz'
          // sh 'scp -i ~/.ssh/ifsdev.pem ./deploy.sh ubuntu@extdev.teamandtech.com:/home/tnt-ts-api/deploy.sh'
          // sh 'ssh -i ~/.ssh/ifsdev.pem ubuntu@extdev.teamandtech.com /bin/sh /home/tnt-ts-api/deploy.sh'
        }
        echo 'Publish step'
        script {
          if ( "$GIT_BRANCH" == "dev" ) {
            echo 'Publish step disabled'
          } else {
            echo "Not publishing $GIT_BRANCH"
          }
        }
      }
    }
  }
}