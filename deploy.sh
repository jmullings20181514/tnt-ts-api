#!/bin/bash -x
echo "Deploying the application"
cd /home/tnt-ts-api 
rm -rf /home/tnt-ts-api/deployed
mkrdir deployed
tar -xvzf tnt-ts-api.tgz -C /deployed
#[ -d deployed ] && cd deployed && npm run undeploy && cd .. && rm -rf /home/tnt-ts-api/deployed
#mv package deployed
cd deployed
npm install
npm run build
npm start