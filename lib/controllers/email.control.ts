import { Request, Response } from 'express';
import * as nodemailer from 'nodemailer';
import FileService from '../services/file.service';

const transporter = nodemailer.createTransport({
  host: 'in-v3.mailjet.com',
  port: 587,
  auth: {
    user: '8ba70e0ce4dd520a378654b2d00166c8',
    pass: '499f9b2adee3d9d6f57b74c2c763783e'
  }
});

export class EmailController {
  public async sendMail(req: Request, res: Response) {
    try {
      const opt = {
        from: 'judd.sosa@teamandtech.com',
        to: req.body.to,
        subject: `Form Output`,
        // html: `Please see attachment`,
        text: 'Please see attachment',
        attachments: []
      };

      // generate pdf
      const pdfBuffer = await FileService.generatePDF(req.body.message);
      opt.attachments.push({
        filename: 'attachment.pdf',
        content: pdfBuffer
      });

      // generate docx
      // data should follow the format of the docx template
      const docxBuffer = await FileService.generateDocx({
        message: req.body.message
      });
      opt.attachments.push({
        filename: 'attachment2.docx',
        content: docxBuffer
      });
      await transporter.sendMail(opt);
      res.json({ message: 'Email Sent' });
    } catch (e) {
      console.error(e);
      res.status(500).json({ message: 'Failed to send email.' });
    }
  }
}
