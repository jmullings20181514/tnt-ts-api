import { Request, Response } from 'express';
import { FormService } from '../services/form.service';
const formService = new FormService();

export class FormController {

  public addNewForm(req: Request, res: Response) {
    try {
      formService.addFormByName(req, res);
    } catch (e) {
      res.json('invalid / corrupt data');
    }
  }

  public getForm(req: Request, res: Response) {
    try {
      formService.getAllForms(req, res);
    } catch (e) {
      res.json('invalid / corrupt data');
    }
  }

  public getFormByName(req: Request, res: Response) {
    try {
      formService.getOneForm(req, res);
    } catch (e) {
      res.json('invalid / corrupt data');
    }
  }
}
