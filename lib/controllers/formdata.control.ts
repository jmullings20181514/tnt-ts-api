import { Request, Response } from 'express';
import { RunAsync } from '../services/runAsync';

const runAsync = new RunAsync();

export class FormDataController {
  // public addNewForm(req: Request, res: Response) {
  //   try {
  //     runAsync.guardAddData(req, res);
  //   } catch (e) {
  //     res.json('invalid / corrupt data');
  //   }
  // }

  // public getFormByName(req: Request, res: Response) {
  //   try {
  //     runAsync.guardPullData(req, res);
  //   } catch (e) {
  //     res.json('data does not exist');
  //   }
  // }

  public getElements(req: Request, res: Response) {
    try {
      runAsync.getElements(req, res);
    } catch (e) {
      res.json('data does not exist');
    }
  }

  public searchElements(req: Request, res: Response) {
    try {
      runAsync.searchElements(req, res);
    } catch (e) {
      res.json('data does not exist');
    }
  }
}
