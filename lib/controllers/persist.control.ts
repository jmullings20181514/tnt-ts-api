import { Request, Response } from 'express';
import { PersistService } from '../services/persist.service';
import { RunAsync } from '../services/runAsync';

const persistService = new PersistService();
const runAsync = new RunAsync();

export class PersistController {
  public addPersist(req: Request, res: Response) {
    try {
      persistService.upsertPersist(req, res);
    } catch (e) {
      res.json(e);
    }
  }

  public getPersist(req: Request, res: Response) {
    try {
      persistService.getAllPersist(req, res);
    } catch (e) {
      res.json(e);
    }
  }

  public getPersistByName(req: Request, res: Response) {
    try {
      persistService.getPersist(req, res);
    } catch (e) {
      res.json(e);
    }
  }

  public deletePersist(req: Request, res: Response) {
    try {
      persistService.deletePersist(req, res);
    } catch (e) {
      res.json(e);
    }
  }

  public submitChips(req: Request, res: Response) {
    try {
      runAsync.submitChips(req, res);
    } catch (e) {
      res.json(e);
    }
  }

  public addChipsToPersist(req: Request, res: Response) {
    try {
      runAsync.addChipsToPersist(req, res);
    } catch (e) {
      res.json(e);
    }
  }

  public updateChipsField(req: Request, res: Response) {
    try {
      runAsync.updateChipsField(req, res);
    } catch (e) {
      res.json(e);
    }
  }

  public deleteChipsQueue(req: Request, res: Response) {
    try {
      runAsync.deleteChips(req, res);
    } catch (e) {
      res.json(e);
    }
  }

  public returnChip(req: Request, res: Response) {
    try {
      runAsync.returnChip(req, res);
    } catch (e) {
      res.json(e);
    }
  }

  public returnAllChips(req: Request, res: Response) {
    try {
      runAsync.returnAllChips(req, res);
    } catch (e) {
      res.json(e);
    }
  }

  public reformQueue(req: Request, res: Response) {
    try {
      runAsync.reformQueue(req, res);
    } catch (e) {
      res.json(e);
    }
  }

  public pullForeignData(req: Request, res: Response) {
    try {
      runAsync.pullForeignData(req, res);
    } catch (e) {
      res.json(e);
    }
  }

  public checkDataIntegrity(req: Request, res: Response) {
    try {
      runAsync.checkDataIntegrity(req, res);
    } catch (e) {
      res.json(e);
    }
  }

  public cleanData(req: Request, res: Response) {
    try {
      runAsync.cleanData(req, res);
    } catch (e) {
      res.json(e);
    }
  }

}
