import { Request, Response } from 'express';
import { EmailController } from '../controllers/email.control';
import { FormController } from '../controllers/form.control';
import { FormDataController } from '../controllers/formdata.control';
import { PersistController } from '../controllers/persist.control';

export class Routes {
    public formController: FormController = new FormController();
    public formDataController: FormDataController = new FormDataController();
    public persistController: PersistController = new PersistController();
    public emailController: EmailController = new EmailController();

    public routes(app): void {

        /**
         * @api {get} {endpoint}/ Test
         * @apiVersion 1.0.0
         * @apiDescription Tests api connection
         * @apiSuccessExample {json} Success response example:
         * {
         * "message": "GET request successfulll!!!!"
         * }
         *  @apiErrorExample {json} Error response example:
         * {"error": "Failed to Send Message!"}
         */
        app.route('/')
            .get((req: Request, res: Response) => {
                try {
                    res.status(200).json({
                        message: 'GET request successfulll!!!!'
                    });
                } catch (e) {
                    console.log('ERROR -->', e);
                    res.status(500).json({
                        error: e
                    });
                }
            });

        app.route('/persist')
            /**
             * @api {get} {endpoint}/persist Get All Persisted Data
             * @apiVersion 1.0.0
             * @apiGroup Persist
             * @apiDescription Get all persisted data
             * @apiSuccessExample {json} Success response example:
             * [{ "_id": "5c76857050e68dbba1f58ae0","reference": "0x2261646d696e40757365722e636f6d22",
             * "__v": 0, "persist": "0x7b226e616d65223a22222c22656d61696c223a2261646d696e40757365722e63
             * 6f6d222c2270617373776f7264223a2261646d696e3132333435222c22706172616d73223a7b7d2c2266726f6
             * d223a22222c22757365724964223a6e756c6c2c22757365724e616d65223a22222c2263757272656e74466f726
             * d223a22222c2263757272656e74446174614964223a22222c226368697073223a5b5d2c22617069223a2268747
             * 470733a2f2f6170696465762e7465616d616e64746563682e636f6d2f6966732f7375626d6974222c22746f223a22227d"
             * }]
             * @apiErrorExample {json} Error response example:
             * []
             */
            .get(this.persistController.getPersist);

        app.route('/persist/:persistId')
            /**
             *  @api {post} {endpoint}/persist/{persistId} Upsert Data to Persist
             * @apiVersion 1.0.0
             *  @apiGroup Persist
             *  @apiDescription Delete persisted data by ID
             *  @apiParam {string} reference - User Reference
             * @apiParam {string} persist - Hexed form data
             *  @apiParamExample {json} Request Example:
             * {"reference": "0x2261646d696e40757365722e636f6d22",
             * "persist": "0x7b226e616d65223a22222c22656d61696c223a2261646d696e
             * 40757365722e636f6d222c2270617373776f7264223a2261646d696e31323334352
             * 22c22706172616d73223a7b7d2c2266726f6d223a22222c22757365724964223a6e7
             * 56c6c2c22757365724e616d65223a22222c2263757272656e74466f726d223a22222c
             * 2263757272656e74446174614964223a22222c226368697073223a5b5d2c226170692
             * 23a2268747470733a2f2f6170696465762e7465616d616e64746563682e636f6d2f696
             * 6732f7375626d6974222c22746f223a22227d"}
             * @apiSuccessExample {json} Success response example:
             * { "_id": "5c76857050e68dbba1f58ae0",
             * "reference": "0x2261646d696e40757365722e636f6d22",
             *  "__v": 0,
             * "persist": "0x7b226e616d65223a22222c22656d61696c223a2261646d696e4075736
             * 5722e636f6d222c2270617373776f7264223a2261646d696e3132333435222c227061726
             * 16d73223a7b7d2c2266726f6d223a22222c22757365724964223a6e756c6c2c227573657
             * 24e616d65223a22222c2263757272656e74466f726d223a22222c2263757272656e744461
             * 74614964223a22222c226368697073223a5b5d2c22617069223a2268747470733a2f2f617
             * 0696465762e7465616d616e64746563682e636f6d2f6966732f7375626d6974222c22746f2
             * 23a22227d" }
             *  @apiErrorExample {json} Error response example:
             *  null
             */
            .post(this.persistController.addPersist)
            /**
             *  @api {get} {endpoint}/persist/{persistId} Get Persisted Data By ID
             *  @apiVersion 1.0.0
             * @apiGroup Persist
             * @apiDescription Get persisted data by ID
             *  @apiSuccessExample {json} Success response example:
             * { "_id": "5c76857050e68dbba1f58ae0",
             *  "reference": "0x2261646d696e40757365722e636f6d22",
             * "__v": 0,
             *  "persist": "0x7b226e616d65223a22222c22656d61696c223a2261
             * 646d696e40757365722e636f6d222c2270617373776f7264223a2261646d696e313
             * 2333435222c22706172616d73223a7b7d2c2266726f6d223a22222c22757365724964
             * 223a6e756c6c2c22757365724e616d65223a22222c2263757272656e74466f726d223
             * a22222c2263757272656e74446174614964223a22222c226368697073223a5b5d2c2261
             * 7069223a2268747470733a2f2f6170696465762e7465616d616e64746563682e636f6d
             * 2f6966732f7375626d6974222c22746f223a22227d" }
             * @apiErrorExample {json} Error response example:
             * null
             */
            .get(this.persistController.getPersistByName)
            /**
             * @api {delete} {endpoint}/persist/{persistId} Delete Persisted Data by ID
             * @apiVersion 1.0.0
             * @apiGroup Persist
             *  @apiDescription Delete persisted data by ID
             * @apiSuccessExample {json} Success response example:
             * {"_id": "5c76857050e68dbba1f58ae0",
             * "reference": "0x2261646d696e40757365722e636f6d22",
             * "__v": 0,
             * "persist": "0x7b226e616d65223a22222c22656d61696c223a
             * 2261646d696e40757365722e636f6d222c2270617373776f7264
             * 223a2261646d696e3132333435222c22706172616d73223a7b7d
             * 2c2266726f6d223a22222c22757365724964223a6e756c6c2c227
             * 57365724e616d65223a22222c2263757272656e74466f726d223a
             * 22222c2263757272656e74446174614964223a22222c226368697
             * 073223a5b5d2c22617069223a2268747470733a2f2f617069646576
             * 2e7465616d616e64746563682e636f6d2f6966732f7375626d69742
             * 22c22746f223a22227d"}
             * @apiErrorExample {json} Error response example:
             * null
             */
            .delete(this.persistController.deletePersist);

        app.route('/chips')
            /**
             * @api {post} {endpoint}/chips Trigger Submit for Cached Data
             *  @apiVersion 1.0.0
             *
             *  @apiGroup Chips
             * @apiDescription Submit cached data by ID
             * @apiParam {string} navState - Hexed navState data
             * @apiParamExample {json} Request Example:
             * {navState": "0x2261646d696e40757365722e636f6d22",}
             *  @apiSuccessExample {json} Success response example:
             * {severity: 'success', summary: 'Success', detail: 'Send docs: '
             * + results.sendDocs + '; Post: ' + results.completeApiPost + '\nDeleting entry now.' }
             * @apiErrorExample {json} Error response example:
             *  { severity: 'error', summary: 'Error', detail: 'Entry not submitted, error found' }
             */
            .post(this.persistController.submitChips);

        app.route('/chips/:clientId')
            /**
             * @api {get} {endpoint}/chips/{clientId} Get Cached Data by ID
             * @apiVersion 1.0.0
             * @apiGroup Chips
             * @apiDescription Get cached data by ID
             * @apiSuccessExample {json} Success response example:
             * { severity: 'success', summary: 'Success',
             * detail: {"API_ClientForm":[{"formData":{"_id":"5be1d0cdae6926e7da7f5dfb",
             * "email":"petra.moser@vetcompandpen.com","ssnKey":"M7777_Moser_Petra",
             * "ssn":"777777","phone":"(352) 888-6600","lastName":"Moser","firstName":"Petra",
             * "forms":"[{\"name\":\"API_addresses\",\"id\":\"5be1d0cdae6926e7da7f5dfb\"}]","_lastInit":"M","_lastfour":"7777"}}]},
             * chips: [{\"id\" : ObjectId(\"5be1d0cdae6926e7da7f5dfb\"),\"name\" : \"API_ClientForm\", \"complete\" : 1}] }
             * @apiErrorExample {json} Error response example:
             * { severity: 'error', summary: 'Error', detail: 'Data not found' }
             */
            .get(this.persistController.returnAllChips)
            /**
             * @api {post} {endpoint}/chips/{clientId} Add FormData and Answers to Cache
             * @apiVersion 1.0.0
             * @apiGroup Chips
             * @apiDescription Insert cached data by ID
             *
             * @apiParam {string} navState - Hexed navState data
             *
             * @apiParamExample {json} Request Example:
             * { "persist": "0x2261646d696e40757365722e636f6d22",
             *   "navState": "0x2261646d696e40757365722e636f6d22",
             * "formDetails": "0x2261646d696e40757365722e636f6d22"
             * }
             * @apiSuccessExample {json} Success response example:
             * { severity: 'success', summary: 'Success', detail: API_ClientForm + ' for updating',
             *  chips: [{\"id\" : ObjectId(\"5be1d0cdae6926e7da7f5dfb\"),\"name\" : \"API_ClientForm\", \"complete\" : 1}] }
             * @apiErrorExample {json} Error response example:
             * { severity: 'error', summary: 'Error', detail: 'Incomplete data sent' }
             */
            .post(this.persistController.addChipsToPersist)
            /**
             * @api {put} {endpoint}/chips/{clientId} Update Existing Cache
             * @apiVersion 1.0.0
             *  @apiGroup Chips
             * @apiDescription Update cached data by ID
             *  @apiParam {string} navState - Hexed navState data
             * @apiParamExample {json} Request Example:
             * {
             * "navState": "0x7b226e616d65223a22222c22656d61696c223a22737570657240757365722e636f6d222c2270617373776f
             * 7264223a2273757065723132333435222c22706172616d73223a7b7d2c2266726f6d223a22222c22757365724964223a6e756c
             * 6c2c22757365724e616d65223a225375706572222c2263757272656e74466f726d223a224150495f436c69656e74466f726d22
             * 2c2263757272656e74466f726d4964223a22356337653839613035306536386462626131353038613831222c2263757272656e7
             * 4446174614964223a22356233323563313066663966363336323062313136353663222c226368697073223a5b225072696e7422
             * 2c225465616d73225d2c22617069223a2268747470733a2f2f6170696465762e7465616d616e64746563682e636f6d2f6966732f
             * 7375626d6974222c22746f223a22227d",
             * "chip": "0x7b666f726d4e616d653a20274150495f436c69656e74466f726d272c5f69643a202735623332356331306666396636
             * 3336323062313136353663272c6669656c643a20276c6173744e616d65272c76616c75653a20275445535454455354277d",
             * "reference": "0x22737570657240757365722e636f6d22",
             * "options": "0x5b7b225f6964223a22356233323563313066663966363336323062313136353663222c22757365724964223a6e75
             * 6c6c2c22666f726d4964223a22356337653839613035306536386462626131353038613831222c22636c69656e744964223a223562
             * 33323563313066663966363336323062313136353663222c227175657374696f6e4964223a223537326663356536386661316264303
             * 03030666262333266222c22616e73776572223a22353535222c2273696e676c65416e73776572223a22353535222c22646174654372
             * 6561746564223a313535323939333932303530312c226461746555706461746564223a313535323939333932383235332c226c61737
             * 4437265617465644279223a225375706572222c226c617374557064617465644279223a225375706572222c22706172656e7452657370
             * 6f6e73654964223a6e756c6c2c226e616d65223a2273736e227d2c7b225f6964223a2235623332356331306666396636333632306231
             * 3136353663222c22757365724964223a6e756c6c2c22666f726d4964223a22356337653839613035306536386462626131353038613831
             * 222c22636c69656e744964223a22356233323563313066663966363336323062313136353663222c227175657374696f6e4964223a2235
             * 3732666335656138666131626430303030666262333330222c22616e73776572223a22435f436c656d6f6e73746573745f416c6c656e22
             * 2c2273696e676c65416e73776572223a22435f436c656d6f6e73746573745f416c6c656e222c226461746543726561746564223a31353532
             * 3939333932303530312c226461746555706461746564223a313535323939333932383235392c226c617374437265617465644279223a225
             * 375706572222c226c617374557064617465644279223a225375706572222c22706172656e74526573706f6e73654964223a6e756c6c2c2
             * 26e616d65223a2273736e4b6579227d5d"
             * }
             * @apiSuccessExample {json} Success response example:
             * { severity: 'success', summary: 'Success', detail: API_ClientForm + 'updated' }
             * @apiErrorExample {json} Error response example:
             * { severity: 'error', summary: 'Error', detail: 'Update failed, please check status of API' }
             */
            .put(this.persistController.updateChipsField)
            /**
             *
             * @api {delete} {endpoint}/chips/{clientId} Delete Cached Data by ID
             * @apiVersion 1.0.0
             * @apiGroup Chips
             * @apiDescription Delete cached data by ID
             * @apiSuccessExample {json} Success response example:
             * { severity: 'success', summary: 'Success', detail: 'Entry for 5c76857050e68dbba1f58ae0 has been deleted.' }
             * @apiErrorExample {json} Error response example:
             * { severity: 'error', summary: 'Error', detail: 'Entry for 5c76857050e68dbba1f58ae0 not found, check Client ID' }
             */
            .delete(this.persistController.deleteChipsQueue);

        app.route('/chips/:clientId/:form')
            /**
             *  @api {get} {endpoint}/chips/{clientId}/{form} Get Cached FormData
             * @apiVersion 1.0.0
             * @apiGroup Chips
             * @apiDescription Get cached data by ID and form name
             * @apiSuccessExample {json} Success response example:
             * { severity: 'success', summary: 'Success',
             * detail: [{"formData":{"_id":"5be1d0cdae6926e7da7f5dfb",
             * "email":"petra.moser@vetcompandpen.com","ssnKey":"M7777_Moser_Petra",
             * "ssn":"777777","phone":"(352) 888-6600","lastName":"Moser","firstName":
             * "Petra","forms":"[{\"name\":\"API_addresses\",\"id\":\"5be1d0cdae6926e7da7f5dfb\"}]","_lastInit":"M","_lastfour":"7777"}}]},
             * chips: [{\"id\" : ObjectId(\"5be1d0cdae6926e7da7f5dfb\"),\"name\" : \"API_ClientForm\", \"complete\" : 1}]
             * @apiErrorExample {json} Error response example:
             * { severity: 'error', summary: 'Error', detail: 'Data not found' }
             */
            .get(this.persistController.returnChip);

        app.route('/client/:formName?/:userId?')
            /**
             * @api {get} {endpoint}/client/{formName}?/{userId}? Pull Foreign Data
             * @apiVersion 1.0.0
             * @apiGroup Foreign Data
             * @apiDescription Get foreign data by ID and form name
             * @apiSuccessExample {json} Success response example:
             * {"form": "0x5b7b225f6964223a2235623332356331306666396636
             * 3336323062313136353663222c2266697273744e616d65223a22416c6
             * 47065656c657240676d61696c2e636f6d222c2273736e4b6579223a225
             * f5f467269656e64222c2273736e223a22222c2270686f6e65223a22222c2
             * 26c6173744e616d65223a22222c2266697273744e616d65223a22467269656
             * e64227d2c7b225f6964223a223562656465366139333434366131303933343
             * 53466363561222c2266697273744e616d65223a22416c6578"}
             * @apiErrorExample {json} Error response example:
             *  { severity: 'error', summary: 'Error', detail: 'Id is invalid' }
             */
            .get(this.persistController.pullForeignData);

        app.route('/check/:formName')
            /**
             * @api {get} {endpoint}/check/{formName} Check Data Integrity
             * @apiVersion 1.0.0
             * @apiGroup Foreign Data
             * @apiDescription Check integrity and correctness of foreign data
             * @apiSuccessExample {json} Success response example:
             * {"form": "0x5b7b225f6964223a22356233323563313066663"}
             * @apiErrorExample {json} Error response example:
             * { severity: 'error', summary: 'Error', detail: 'Id is invalid' }
             */
            .get(this.persistController.checkDataIntegrity);

        /////////////  FORM Routes //////////
        app.route('/form')
            /**
             * @api {get} {endpoint}/form Get All Forms
             * @apiVersion 1.0.0
             * @apiGroup Forms
             * @apiDescription Gets all forms
             * @apiSuccessExample {json} Success response example:
             * {
             *     [
             *     {
             *         "_id": "5c389c67cc6ee6000455fffd",
             *         "form": "",
             *         "reference": "base_form",
             *         "__v": 0
             *     },
             *     {
             *         "_id": "5c389da6cc6ee6000455ffff",
             *         "form": "",
             *         "reference": "text",
             *         "__v": 0
             *     }
             *     ]
             * }
             * @apiErrorExample {json} Error response example:
             * {
             *     "error": "Cannot GET form"
             * }
             */
            .get(this.formController.getForm);

        app.route('/form/:formId')
            /**
             * @api {post} {endpoint}/form/{formId} Add New Form
             * @apiVersion 1.0.0
             * @apiGroup Forms
             * @apiDescription Upserts form into database
             * @apiParam {string} clientName - Client Name Reference
             * @apiParam {string} form - Hexed form data
             * @apiParam {string} reference - Form Name
             * @apiParamExample {json} Request Example:
             * {
             *     "clientName": "robin"
             *     "form": "0x7b22636c69656e744e"
             *     "reference": "Robin%20123"
             * }
             * @apiSuccessExample {json} Success response example:
             * {
             *     {
             *     "_id":"5c4ea1f4309ea400042c18ac",
             *     "form":"0x7b22636c69656e744e616d65223a22726f",
             *     "clientName":"robin",
             *     "reference":"Robin%20123",
             *     "__v":0
             *     }
             * }
             * @apiErrorExample {json} Error response example:
             * {
             *     "message": "invalid / corrupt data"
             * }
             */
            .post(this.formController.addNewForm)
            /**
             * @api {get} {endpoint}/form/{formId} Get Form By Name
             * @apiVersion 1.0.0
             * @apiGroup Forms
             * @apiDescription Gets form by id
             * @apiSuccessExample {json} Success response example:
             * {
             * {
             *     "_id": "5c389c67cc6ee6000455fffd",
             *     "form": "0xasdsgdrzgxrthrgazwetfsw4r53246te5xsy5sy",
             *     "reference": "base_form",
             *     "__v": 0
             * }
             * }
             * @apiErrorExample {json} Error response example:
             * {
             *     "error": "Form reference does not exist!"
             * }
             */
            .get(this.formController.getFormByName);

        /////////////  DEPRECIATED ROUTES //////////

        app.route('/clean')
            .get(this.persistController.cleanData);

        app.route('/queue/:form/:id')
            .get(this.persistController.reformQueue);

        // app.route('/formdata/:formName/:formId?')
        //     .post(this.formDataController.addNewForm)
        //     .get(this.formDataController.getFormByName);

        app.route('/sendemail')
            .post(this.emailController.sendMail);

        app.route('/elements/:formName?/:clientId?/:startRow?/:endRow?')
            .get(this.formDataController.getElements);

        app.route('/search/:formName?/:answer/:startRow?/:endRow?')
            .get(this.formDataController.searchElements);
    }
}
