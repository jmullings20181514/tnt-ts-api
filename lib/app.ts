import * as cors from 'cors';
import * as dotenv from 'dotenv';
import * as express from 'express';
import * as helmet from 'helmet';

import * as bodyParser from 'body-parser';
import * as mongoose from 'mongoose';
import { Routes } from './routes/routes';

import * as path from 'path';

dotenv.config({ path: '.env' });

class App {

    public app: express.Application;
    public routePrv: Routes = new Routes();
    // public mongoUrl: string = process.env.MONGODB_URI || 'mongodb://v_eugenio:rootDA70@ds157383.mlab.com:57383/tnt-db';
    public mongoUrl = 'mongodb://root:rootDA70@ds157383.mlab.com:57383/tnt-db';
    // public mongoUrl: string = 'mongodb://localhost/tnt-ifs-client';

    constructor() {
        this.app = express();
        this.config();
        this.routePrv.routes(this.app);
        this.mongoSetup();
    }

    private config(): void {
        this.app.use(cors());
        this.app.use(helmet());
        this.app.use(bodyParser.json({ limit: 1024 * 1024 * 1024 * 50 }));
        this.app.use(bodyParser.urlencoded({
            limit: 1024 * 1024 * 1024 * 50,
            extended: true
        }));
        // serving static files
        this.app.use('/tnt-ifs-docs', express.static(path.join(__dirname, '../docs')));
        this.app.use(express.static('public'));

        this.app.use(express.static('public'));
    }

    private mongoSetup(): void {
        (mongoose as any).Promise = global.Promise;
        // mongoose.Promise = global.Promise;
        mongoose.connect(this.mongoUrl);
    }
}

export default new App().app;
