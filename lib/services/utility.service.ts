export class Utility {
    public async getChainRef(body, model) {
        if (body._predRef) {
            return body._predRef;
        } else {
            const forms = await model.find({});
            const output = forms.length ? forms[forms.length - 1]['_id'] : 'head';
            return output;
        }
    }

    public IsJsonString(str) {
        try {
            return JSON.parse(str);
        } catch (e) {
            return false;
        }
    }
}
