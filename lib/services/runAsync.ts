import * as async from 'async';
import { ObjectId } from 'bson';
import { Request, Response } from 'express';
import * as _ from 'lodash';
import * as mongoose from 'mongoose';
import * as request from 'request';
import * as TypeGuard from 'ts-runtime-typeguard';
import { ChipsSchema, FormData, FormSchema, InputElementSchema, PersistSchema } from '../models/model';

const baseCollection = 'generalforms';
const typeGuard = new TypeGuard();

export const mongoData = (name) => {
    const collectionName = name + '_datas';
    return mongoose.model(collectionName, FormData);
};

export const mongoForm = (name) => {
    const collectionName = name;
    return mongoose.model(collectionName, FormSchema);
};

export const persistForm = () => {
    return mongoose.model('persistforms', PersistSchema);
};

export const chipsForm = () => {
    return mongoose.model('chips', ChipsSchema);
};

export const inputElement = () => {
    return mongoose.model('inputElement', InputElementSchema);
};

export const formSchema = {
    formModel: {
        formName: 'formName',
        data: []
    },
    dataModel: {
        form: [],
        schema: {}
    }
};

export const docSchema = {
    appId: String,
    filingKey: String,
    fileName: String,
    formData: {}
};

export class RunAsync {

    public getElements(req: Request, res: Response) {
        const that = this;
        const clientID = req.params.clientId;
        if (!req.params.formName && !clientID) {
            const questionids = {};
            that.pullAllQuestions((err, body) => {
                for (const item of that.IsJsonString(body)) {
                    questionids[item._id] = (!item.name) ? 'undefined' : item.name;
                }
                res.json(questionids);
            });
            // res.json({ severity: 'error', summary: 'Error', detail: 'No Params sent!!' });
            return;
        }
        const MongoForm = mongoForm(baseCollection);
        try {
            async.autoInject({
                getSchema(callback) {
                    MongoForm.find({ reference: req.params.formName }, (err: any, form: any) => callback(err, form));
                },
                questionKeys(getSchema, callback) {
                    const keys = {};
                    if (getSchema.length) {
                        const form = typeGuard.toAscii(getSchema[0]['form']);
                        if (form) {
                            form['formModel']['data'].map((thisData) => keys[thisData.questionId] = thisData.ref);
                        }
                        callback(null, { formId: form._id, keys });
                    } else {
                        callback(null, { formId: '', keys: {} });
                    }
                },
                callData(questionKeys, callback) {
                    const call = (typeof clientID !== 'undefined') ? '&clientId=' + clientID : '';
                    const api = 'https://tnt-mock-node-api.herokuapp.com/input-element?startRow='
                        + req.params.startRow + '&endRow=' + req.params.endRow + 'formId=' + questionKeys['formId'] + call;

                    request.get(api, function optionalCallback(err, httpResponse, body) {
                        callback(err, that.IsJsonString(body));
                    });
                },
                callAllQuestions(callback) {
                    const questionids = {};
                    that.pullAllQuestions((err, body) => {
                        for (const item of that.IsJsonString(body)) {
                            questionids[item._id] = item.name;
                        }
                        callback(err, questionids);
                    });
                },
                matchKeys(callData, getSchema, questionKeys, callAllQuestions, callback) {
                    const keys = _.keys(questionKeys['keys']);
                    let dataReturn;
                    const data = {};
                    const form = {};
                    // console.log('callData', callData)
                    if (callData && keys.length) {
                        // console.log('questionKeys', questionKeys['keys'])
                        for (const item of callData as any) {
                            keys.map((key) => {
                                if (item['questionId'] === key) {
                                    form[item.clientId] = {
                                        _predRef: 0,
                                        _id: item.clientId,
                                        ...form[item.clientId],
                                        [questionKeys['keys'][key]]: item['answer']
                                    };
                                }
                            });
                            data[item.clientId] = {
                                _id: item.clientId,
                                _predRef: 0,
                                form: form[item.clientId]
                            };
                        }
                        const Schema = {
                            form: (getSchema.length) ? typeGuard.toAscii(getSchema[0]['form']) : {},
                            formData: _.values(data)
                        };
                        dataReturn = (typeof clientID === 'undefined') ? { form: typeGuard.toHex(_.values(data)) }
                            : { form: typeGuard.toHex(Schema) };
                    }
                    callback(null, dataReturn);
                }
            }, (err, results) => {
                res.json((err) ? err : results.matchKeys);
            });
        } catch (err) {
            return err;
        }
    }

    public searchElements(req: Request, res: Response) {
        const that = this;
        const answer = req.params.answer;
        const MongoForm = mongoForm(baseCollection);
        try {
            async.autoInject({
                getSchema(callback) {
                    MongoForm.findOne({ reference: req.params.formName }, (err: any, form: any) => callback(err, form));
                },
                questionKeys(getSchema, callback) {
                    const keys = {};
                    if (getSchema) {
                        const form = typeGuard.toAscii(getSchema['form']);
                        if (form) {
                            form['formModel']['data'].map((thisData) => keys[thisData.questionId] = thisData.ref);
                        }
                        callback(null, { formId: form._id, keys });
                    } else {
                        callback(null, { formId: '', keys: {} });
                    }
                },
                querySearch(getSchema, callback) {
                    // const call = (typeof clientID !== 'undefined') ? '&clientId=' + clientID : '';
                    let api = 'https://tnt-mock-node-api.herokuapp.com/input-element';
                    if (req.params.startRow && req.params.endRow && getSchema) {
                        api += '?startRow='
                            + req.params.startRow + '&endRow=' + req.params.endRow + '&answer=' + answer
                            + '&formId=' + getSchema['_id'];
                    } else if (getSchema) {
                        api += '?answer=' + answer
                            + '&formId=' + getSchema['_id'];
                    } else {
                        api += '?answer=' + answer;
                    }
                    request.get(api, function optionalCallback(err, httpResponse, body) {
                        callback(err, that.IsJsonString(body));
                    });
                },
                callData(getSchema, querySearch, callback) {
                    const query = {};
                    if (querySearch) {
                        for (const item of querySearch as any) {
                            query[item.clientId] = `?formId=${getSchema['_id']}&clientId=${item.clientId}`;
                        }
                        async.map(_.values(query), (form, cb) => {
                            request.get('https://tnt-mock-node-api.herokuapp.com/input-element' + form,
                                (err, httpResponse, body) => {
                                    cb(err, that.IsJsonString(body));
                                });
                        }, (e, results) => {
                            callback(e, _.flatten(results));
                        });
                    } else {
                        callback(null, []);
                    }
                },
                callAllQuestions(callback) {
                    const questionids = {};
                    that.pullAllQuestions((err, body) => {
                        for (const item of that.IsJsonString(body)) {
                            questionids[item._id] = item.name;
                        }
                        callback(err, questionids);
                    });
                },
                matchKeys(callData, getSchema, questionKeys, callback) {
                    const keys = _.keys(questionKeys['keys']);
                    const data = {};
                    const form = {};
                    // console.log('callData', callData)
                    if (callData && keys.length) {
                        // console.log('questionKeys', questionKeys['keys'])
                        for (const item of callData as any) {
                            keys.map((key) => {
                                if (item['questionId'] === key) {
                                    form[item.clientId] = {
                                        _predRef: 0,
                                        _id: item.clientId,
                                        ...form[item.clientId],
                                        [questionKeys['keys'][key]]: item['answer']
                                    };
                                }
                            });
                            data[item.clientId] = {
                                _id: item.clientId,
                                _predRef: 0,
                                form: form[item.clientId]
                            };
                        }
                    }
                    const Schema = {
                        form: (getSchema) ? typeGuard.toAscii(getSchema['form']) : {},
                        formData: _.values(data)
                        // dataReturn = (typeof answer === 'undefined') ? { form: typeGuard.toHex(_.values(data)) }
                        //     : { form: typeGuard.toHex(Schema) };
                    };
                    callback(null, { form: typeGuard.toHex(Schema) });
                }
            }, (err, results) => {
                res.json((err) ? err : results.matchKeys);
            });
        } catch (err) {
            return err;
        }
    }

    public getChipsList(form) {
        try {
            if (form && form['schema']) {
                const properties = form.schema['properties'];
                let output = [];
                for (const prop in properties) {
                    if (properties[prop].hasOwnProperty('selectedOpt')) {
                        output.push(properties[prop]['name']);
                        // **************** TO DO *******************//
                        if (properties[prop].hasOwnProperty('enableQueue')) {
                            output = properties[prop]['enableQueue'] ? output : [];
                        }
                    }
                }
                return output;
            }
        } catch (err) {
            console.log('err', err);
        }
    }

    public async updateSingleChip(formName, id, field, value, questionId, CB) {
        const ChipsForm = chipsForm();
        try {
            const data = await ChipsForm.findOne({ _id: id });
            let form;
            let forms;
            let thisForm;
            let formData;
            let schema;
            let answers;
            let thisAnswer;
            if (data) {
                answers = this.IsJsonString(data['answers']);
                forms = this.IsJsonString(data['persist']);
                thisForm = forms[formName][0];
                if (forms && thisForm) {
                    formData = forms[formName][0]['formData'];
                    schema = forms[formName][0]['schema'];
                    if (formData) {
                        formData[field] = value;
                    }
                    thisForm = {
                        ...thisForm,
                        formData,
                        schema
                    };
                    forms[formName][0] = thisForm; // stringify to persist
                }
                if (answers) {
                    thisAnswer = answers.find((item) => item.questionId === questionId);
                    if (thisAnswer) {
                        thisAnswer['answer'] = value;
                    }
                }
                form = { [formName]: [thisForm] };
                ChipsForm.findOneAndUpdate({ _id: id }, {
                    answers: JSON.stringify(answers),
                    persist: JSON.stringify(form)
                }, { new: true }, (err, res) => {
                    res ? CB(null, true) : CB(err, null);
                });
            }
        } catch (e) {
            CB(e, null);
        }
    }

    public updateChipsField(req: Request, res: Response) {
        const that = this;
        req.params = null;
        const chip = typeGuard.toAscii(req.body.chip);
        const navState = typeGuard.toAscii(req.body.navState);
        const payload = typeGuard.toAscii(req.body.options);
        const error = that.validateNavState(navState);
        if (error) {
            console.error('validateNavState error:', error);
            res.json(error);
            return;
        }
        try {
            if (chip && payload && navState.currentDataId) {
                async.autoInject({
                    updateSingleChip(callback) {
                        that.updateSingleChip(chip['formName'], chip['_id'], chip['field'], chip['value'], payload[0]['questionId'],
                            (err, confirm) => {
                                callback(err, confirm);
                            });
                    },
                    validateInputElements(callback) {
                        that.validateInputElements(payload, (errors, result) => {
                            callback(errors, result);
                        });
                    },
                    updateInputElement(callback) {
                        const call = 'https://tnt-mock-node-api.herokuapp.com/input-element';
                        request.post(call, { json: { formData: JSON.stringify(payload) } },
                            function optionalCallback(err, httpResponse, body) {
                                callback(err, httpResponse.statusCode);
                            });
                    }
                }, (err, results) => {
                    switch (true) {
                        case results.updateInputElement === 500:
                            res.json({ severity: 'error', summary: 'Error', detail: 'Error sending to input-element!!' });
                            break;
                        case results.updateSingleChip === true:
                            res.json({ severity: 'success', summary: 'Success', detail: chip['formName'] + ' updated!' });
                            break;
                        case err !== null:
                            res.json({ severity: 'error', summary: 'Error', detail: 'Unexpected error in focus out!!' });
                            break;
                        default:
                            res.json({ severity: 'success', summary: 'Success', detail: chip['formName'] + ' updated!' });
                            break;
                    }
                });
            } else {
                res.json({ severity: 'error', summary: 'Error', detail: 'Incorrect Data sent!!' });
            }
        } catch (e) {
            res.json({ severity: 'error', summary: 'Error', detail: e });
        }
    }

    public addChipsToPersist(req: Request, res: Response) {
        try {
            req.params = null;
            const chips = typeGuard.toAscii(req.body.persist);
            const navState = typeGuard.toAscii(req.body.navState);
            const questionPairs = typeGuard.toAscii(req.body.formDetails);
            if (chips && navState.currentFormId && questionPairs.length) {
                this.upsertChip(chips, navState, questionPairs, (remainingQueue) => {
                    res.json({
                        severity: 'success', summary: 'Success', detail: navState.currentForm +
                            ' for updating', chips: JSON.stringify(remainingQueue)
                    });
                });
            } else {
                res.json({ severity: 'error', summary: 'Error', detail: 'Incomplete data sent' });
            }
        } catch (err) {
            res.json({ severity: 'error', summary: 'Error', detail: err });
        }
    }

    public upsertChip(chips, navState, questionPairs, cb) {
        const that = this;
        const chipsOutput = this.getChipsList(chips) as any;
        const formData = chips.formData;
        // transform formData => answers
        const parsed = this.IsJsonString(formData[chipsOutput]);
        const chipsQueue = (chipsOutput.length && parsed) ? parsed : [];
        const ChipsForm = chipsForm();
        const formName = navState.currentForm;
        navState.currentDataId = navState.currentDataId ? navState.currentDataId : new ObjectId();
        const clientId = navState.currentDataId;
        const MongoForm = mongoForm(baseCollection);
        try {
            async.autoInject({
                getCurrentFormSchema(callback) {
                    MongoForm.find({ reference: navState.currentForm }, (err: any, form: any) => callback(err, form));
                },
                syncQuestionIds(getCurrentFormSchema, callback) {
                    const form = typeGuard.toAscii(getCurrentFormSchema[0]['form']);
                    const answers = [];
                    form.formModel.data.forEach((fm) => {
                        if (formData[fm.ref]) {
                            answers.push({
                                userId: navState.userId,
                                formId: navState.currentFormId,
                                clientId: navState.currentDataId,
                                questionId: fm.questionId,
                                dateUpdated: Math.floor(+new Date()),
                                lastUpdatedBy: navState.name,
                                dateCreated: Math.floor(+new Date()),
                                lastCreatedBy: navState.name,
                                answer: formData[fm.ref],
                                singleAnswer: formData[fm.ref]
                            });
                        }
                    });
                    console.log('answers', answers);
                    callback(null, answers);
                },
                returnForm(syncQuestionIds, callback) {
                    ChipsForm.findOne({ _id: clientId }, (err: any, form: any) => {
                        if (form) {
                            const oldAnswers = that.IsJsonString(form.answers);
                            // TODO sync duplicates
                            //
                            form.answers = JSON.stringify([...oldAnswers, ...syncQuestionIds]);
                            form.save((thisError) => {
                                callback(thisError, form);
                            });
                        } else {
                            callback(null, null);
                        }
                    });
                },
                // returnForm(callback) {
                //     ChipsForm.findOne({ _id: clientId }, (err: any, form: any) => {
                //         callback(err, form);
                //     });
                // },
                updateChip(returnForm, callback) {
                    console.log('updated chips', returnForm);
                    let newData;
                    let queue;
                    let newQueue;
                    let answers = [];
                    let entirePersist = {};
                    let currentFormData;
                    chipsQueue.push({ name: formName });
                    entirePersist = { [formName]: [{ formData: {} }] };
                    if (returnForm) {
                        entirePersist = that.IsJsonString(returnForm['persist']);
                        queue = returnForm['queue'];
                        answers = that.IsJsonString(returnForm['answers']);
                        currentFormData = (entirePersist[formName] && entirePersist[formName].length)
                            ? entirePersist[formName][0]['formData'] : {};
                    }
                    newData = currentFormData ? { ...currentFormData, ...formData } : formData;
                    if (chipsOutput.length) {
                        const chipContent = that.IsJsonString(newData[chipsOutput[0]]);
                        if (chipContent) {
                            chipContent.forEach((cl) => {
                                cl.id = navState.currentDataId || 0;
                            });
                        }
                    }
                    newQueue = queue ? [...queue, ...chipsQueue] : chipsQueue;
                    newQueue = that.getUnique(newQueue, 'name');
                    if (newQueue && newQueue.length) {
                        newQueue.forEach((cl) => {
                            cl.id = navState.currentDataId || 0;
                        });
                    }
                    for (const item of questionPairs) {
                        delete item['_id'];
                        for (const answer of answers) {
                            if (item['questionId'] === answer['questionId']) {
                                answer['answer'] = item['answer'];
                            }
                        }
                    }
                    if (answers.length === 0) {
                        answers = questionPairs;
                    }
                    /// PLEASE CHECK formName source ??? TODO
                    entirePersist = { ...entirePersist, [formName]: [{ formData: newData }] };
                    for (const item of answers) {
                        item['userId'] = navState.userId;
                        item['formId'] = navState.currentFormId;
                        item['clientId'] = navState.currentDataId;
                        item['dateUpdated'] = Math.floor(+new Date());
                        item['lastUpdatedBy'] = navState.name;
                        item['dateCreated'] = item['dateCreated'] ? item['dateCreated'] : Math.floor(+new Date());
                        item['lastCreatedBy'] = item['lastCreatedBy'] ? item['lastCreatedBy'] : navState.name;
                    }

                    const dataStruct = {
                        _id: navState.currentDataId,
                        persist: JSON.stringify(entirePersist),
                        queue: newQueue,
                        answers: JSON.stringify(answers)
                    };
                    if (returnForm || !navState.currentDataId) {
                        delete dataStruct['_id'];
                    }
                    ChipsForm.findOneAndUpdate({ _id: navState.currentDataId }, dataStruct,
                        { new: true, upsert: true }, (err: any, form: any) => {
                            callback(err, form);
                        });
                },
                pullFormTypeGuard(callback) {
                    MongoForm.findOne({ reference: formName }, (err: any, form: any) => {
                        callback(err, form);
                    });
                },
                injectIdToChips(updateChip, pullFormTypeGuard, callback) {
                    const chipContent = [];
                    const data = that.IsJsonString(updateChip['persist']);
                    const form = typeGuard.toAscii(pullFormTypeGuard['form']);
                    if (updateChip) {
                        if (updateChip['queue'].length) {
                            updateChip['queue'].forEach((cl) => {
                                const chip = {
                                    id: updateChip['_id'],
                                    name: cl['name'],
                                    complete: cl['complete'] || 0
                                };
                                const _values = _.values(data[cl['name']]);
                                if (_values.length && cl['name'] === formName) {
                                    delete _values[0]['formData']['_id'];
                                    chip.complete = typeGuard.checkType(form['dataModel'], _values[0]['formData'], 'schema') ? 1 : 0;
                                }
                                chipContent.push(chip);
                            });
                        }
                    }
                    ChipsForm.findOneAndUpdate({ _id: updateChip['_id'] }, { queue: chipContent }, (errors: any, chip: any) => {
                        callback(errors, chipContent);
                    });
                }
            }, (err, results) => {
                cb(results.injectIdToChips);
            });
        } catch (err) {
            return err;
        }
    }

    public injectIdToChips(queue, id) {
        queue.forEach((cl) => {
            cl.id = id || 0;
        });
        return queue;
    }

    public getUnique(arr, comp) {
        const unique = arr
            .map((e) => e[comp])
            .map((e, i, final) => final.indexOf(e) === i && i)
            .filter((e) => arr[e]).map((e) => arr[e]);
        return unique;
    }

    public upsertFunction(MongoForm, MongoData, newData, collection, getIdFromPersist, CB) {
        try {
            const payload = {
                _id: getIdFromPersist['_id'],
                _predRef: getIdFromPersist['_predRef'],
                form: typeGuard.toHex(newData),
                reference: collection
            };
            async.autoInject({
                get_schema(callback) {
                    MongoForm.findOne({ reference: payload.reference },
                        (err: any, form: any) => {
                            const dataSet = (form) ? typeGuard.toAscii(form.form) : { dataModel: {} };
                            callback(err, dataSet.dataModel);
                        });
                },
                getPredRef(callback) {
                    payload['_predRef'] ? callback(null, payload['_predRef']) :
                        MongoData.find({}, (err: any, form: any) => {
                            callback(err, form.length ? form[form.length - 1]['_id'] : 'head');
                        });
                },
                typeguard(get_schema, callback) {
                    typeGuard.checkType(get_schema, typeGuard.toAscii(payload.form), 'schema')
                        ? callback(null, true) : callback(false, null);
                },
                upsertData(typeguard, getPredRef, callback) {
                    if (typeguard) {
                        payload['_predRef'] = getPredRef;
                        let query = { _id: new ObjectId() };
                        if (payload._id) {
                            query = { _id: payload._id };
                            delete payload._id;
                        }
                        MongoData.findOneAndUpdate(query, payload, { new: true, upsert: true },
                            (err: any, form: any) => callback(err, form));
                    } else {
                        callback('Data does not match schema', null);
                    }
                }
            }, (err, results) => {
                CB(err, results.upsertData['form']);
            });
        } catch (err) {
            return err;
        }
    }

    public apiInterface(data, api, clientId, callback) {
        const payload = [];
        for (const item of data) {
            delete item._id;
            if (item.formId === '' || item.formId == null) {
                item.formId = null;
            }
            if (item.clientId === '' || item.clientId == null) {
                item.clientId = clientId || item.userId;
            }
            // payload.push(typeGuard.toHex(item));
            payload.push(item);
        }
        if (data && api) {
            request.post(api, { form: { formData: JSON.stringify(payload) } }, function optionalCallback(err, httpResponse, body) {
                // console.log('api:', api, { form: { formData: JSON.stringify(payload) } },
                // 'httpResponse.statusCode', httpResponse.statusCode);
                // console.log('api:', api, 'statusCode', httpResponse.statusCode);
                // console.log('httpResponse', JSON.parse(httpResponse.body));
                callback(null, httpResponse.body);
            });
        } else {
            callback(null, null);
        }
    }

    public validateNavState(navState) {
        if (!navState) {
            return { severity: 'error', summary: 'Error', detail: 'No Nav State Provided' };
        }
        if (!navState.currentDataId) {
            return { severity: 'error', summary: 'Error', detail: 'Incorrect Nav State: Missing Current Data Id' };
        }
        if (!navState.currentFormId) {
            return { severity: 'error', summary: 'Error', detail: 'Incorrect Nav State: Missing Current Form Id' };
        }
        if (!navState.userId) {
            return { severity: 'error', summary: 'Error', detail: 'Incorrect Nav State: Missing User Id' };
        }
        if (!navState.to) {
            return { severity: 'error', summary: 'Error', detail: 'Incorrect Nav State: Send To' };
        }
    }

    public async validateInputElements(data, callback) {
        if (!Array.isArray(data)) {
            data = [data];
        }
        const inputElementModel = inputElement();
        async.map(data, (ie, cb) => {
            const model = new inputElementModel(ie);
            model.validate((err) => {
                if (err) {
                    console.error('validateInputElements error:', err);
                    let errors = [];
                    const errorKeys = Object.keys(err.errors);
                    errors = errorKeys.map((key) => {
                        return err.errors[key];
                    });
                    cb(errors.join(', ') as any, null);
                } else {
                    cb(err, ie);
                }
            });
        }, (e, results) => {
            e ? callback(e, null) : callback(null, results);
        });
    }

    public async submitChips(req: Request, res: Response) {
        try {
            const ChipsForm = chipsForm();
            let updateQueue;
            const that = this;
            const navState = typeGuard.toAscii(req.body.navState);
            console.log('navState', navState);
            const error = that.validateNavState(navState);
            if (error) {
                console.error('validateNavState error:', error);
                res.json(error);
                return;
            }
            async.autoInject({
                getIdFromPersist(callback) {
                    ChipsForm.findOne({ _id: navState.currentDataId }, (err: any, form: any) => callback(err, form));
                },
                upsertFromPersist(getIdFromPersist, callback) {
                    console.log('getIdFromPersist', getIdFromPersist);
                    if (getIdFromPersist) {
                        updateQueue = that.IsJsonString(getIdFromPersist['answers']);
                        if (updateQueue) {
                            callback(null, updateQueue);
                        } else {
                            callback({ severity: 'error', summary: 'Error', detail: 'No queue found' }, null);
                        }
                    } else {
                        callback('Queue not found', null);
                    }
                },
                validateAnwsers(upsertFromPersist, callback) {
                    that.validateInputElements(upsertFromPersist, (err, results) => {
                        callback(err, results);
                    });
                },
                checkNewForm(getIdFromPersist, callback) {
                    const persist = that.IsJsonString(getIdFromPersist['persist']);
                    if (persist) {
                        const formKeys = Object.keys(persist);
                        async.map(formKeys, (form, cb) => {
                            request.get('https://apidev.teamandtech.com/ifs/form?name=' + form,
                                (err, httpResponse, body) => {
                                    cb(err, body['_id']);
                                });
                        }, (e, results) => {
                            callback(e, results);
                        });
                    }
                },
                // matchForms(validateAnwsers, checkNewForm, callback) {
                //     const inFormList = [];
                //     const outFormList = [];
                //     const formIdCache = {};

                //     for (const item of validateAnwsers) {
                //         formIdCache[item['formId']] = item['formId'];
                //     }
                //     const formKeys = Object.keys(formIdCache)
                //     for (const formId of formKeys) {
                //         const index = formList.findIndex(fl => fl._id === formIdCache[formId]);
                //         (index !== -1) ? inFormList.push(formId) : outFormList.push(formId)
                //     }
                //     // request.get('https://apidev.teamandtech.com/ifs/form', (err, httpResponse, body) => {
                //     //     const formList = that.IsJsonString(body);

                //     callback(err, 'In form list: ' + inFormList.join(', ')
                //         + '; Not in form list: ' + outFormList.join(', '));
                //     // });
                // },
                createInputElements(validateAnwsers, callback) {
                    console.log('We are sending this to our mock api: ', validateAnwsers);
                    request.post('https://tnt-mock-node-api.herokuapp.com/input-element',
                        { json: { formData: JSON.stringify(validateAnwsers) } }, function optionalCallback(err, httpResponse, body) {
                            console.log('We are getting this back from us: ', body);
                            callback(null, httpResponse.statusCode);
                        });
                },
                completeApiPost(validateAnwsers, callback) {
                    // if (checkNewForm.length) {
                    // const filteredAnswers = validateAnwsers.filter(answer => checkNewForm.includes(answer.formId))
                    console.log('We are sending this to you: ', validateAnwsers);
                    that.apiInterface(validateAnwsers, navState.api, navState.currentDataId, (err, body) => {
                        console.log('We are getting this back from you: ', body);
                        callback(null, body);
                    });
                    // }
                },
                // sendDocs(validateAnwsers, callback) {
                //     let docsAPI = 'https://apidev.teamandtech.com/ifs/form-data/'
                // + navState.currentDataId + '/' + navState.currentFormId;
                //     that.apiInterface(validateAnwsers, 'https://tnt-mock-node-api.herokuapp.com/input-element/'
                // , navState.currentDataId, (err, statusCode) => {
                //         callback(null, statusCode);
                //     })
                // },
                completeEmail(validateAnwsers, callback) {
                    if (validateAnwsers && navState.to) {
                        const array = validateAnwsers as any;
                        // TODO send separate forms
                        request.post('https://tnt-ts-api.herokuapp.com/sendemail', {
                            form: {
                                to: navState.to,
                                message: array.map((data) => data.answer).join(', ')
                            }
                        }, (err, httpResponse, body) => {
                            console.error('completeEmail', err);
                            callback(err, 200);
                        });
                    } else {
                        callback(null, null);
                    }
                }
            }, (err, results) => {
                if (!err && results.getIdFromPersist) {
                    // if (results.sendDocs === '200' && results.completeApiPost === '200'){
                    // console.log('results.sendDocs', results.sendDocs)
                    // if (results.completeApiPost === 200) {
                    res.json({
                        severity: 'success', summary: 'Success',
                        detail: 'completeApiPost: ' + results.completeApiPost + '\n'
                    });
                    // ChipsForm.findOneAndDelete({ _id: results.getIdFromPersist['_id'] },
                    //     (error, confirm) => console.log(error, confirm));
                    // } else {
                    //     res.json({
                    //         severity: 'warning', summary: 'Warning',
                    //         detail: 'Send docs: ' + results.sendDocs + '; Post: ' + results.completeApiPost
                    //             + '\nRemote API might be unavailable. Entry not cleared.'
                    //     });
                    // }
                } else {
                    res.json({ severity: 'error', summary: 'Error', detail: err || 'Entry not submitted, error found' });
                }
            });
        } catch (err) {
            return err;
        }
    }

    public deleteChips(req: Request, res: Response) {
        const ChipsForm = chipsForm();
        ChipsForm.findOneAndDelete({ _id: req.params.clientId }, (err: any, confirm: any) => {
            res.json((!confirm || err) ? {
                severity: 'error', summary: 'Error', detail: err || 'Entry for '
                    + req.params.clientId + ' not found, check Client ID'
            }
                : { severity: 'success', summary: 'Success', detail: 'Entry for ' + req.params.clientId + 'has been deleted.' });
        });
    }

    public returnChip(req: Request, res: Response) {
        const ChipsForm = chipsForm();
        try {
            if (!req.params.clientId || !req.params.form) {
                res.json({ severity: 'error', summary: 'Error', detail: 'IDs not found!' });
            } else {
                ChipsForm.findOne({ _id: req.params.clientId }, (err: any, form: any) => {
                    if (!err && form && form['persist']) {
                        const data = this.IsJsonString(form['persist']);
                        if (data) {
                            res.json({
                                severity: 'success', summary: 'Success',
                                detail: data[req.params.form], chips: JSON.stringify(form['queue'])
                            });
                        }
                    } else {
                        res.json({ severity: 'error', summary: 'Error', detail: err || 'Data not found' });
                    }
                });
            }
        } catch (e) {
            res.json({ severity: 'error', summary: 'Error', detail: e });
        }
    }

    public returnAllChips(req: Request, res: Response) {
        const ChipsForm = chipsForm();
        try {
            ChipsForm.findOne({ _id: req.params.clientId }, (err: any, form: any) => {
                if (!err && form) {
                    const data = this.IsJsonString(form['persist']);
                    if (data) {
                        res.json({
                            severity: 'success', summary: 'Success', detail: JSON.stringify(data),
                            chips: JSON.stringify(form['queue']), persistId: form['_id']
                        });
                    }
                } else {
                    res.json({ severity: 'error', summary: 'Error', detail: err || 'Data not found' });
                }
            });
        } catch (e) {

            res.json({ severity: 'error', summary: 'Error', detail: e });
        }
    }

    public async reformQueue(req: Request, res: Response) {
        let chips;
        let schema;
        let formData;
        let chipsOutput;
        try {
            const MongoForm = mongoForm(baseCollection);
            const MongoData = mongoData(req.params.form);
            const form = await MongoForm.findOne({ reference: req.params.form });
            const data = await MongoData.findOne({ _id: req.params.id });
            if (form) {
                schema = typeGuard.toAscii(form['form']);
                chipsOutput = this.getChipsList(schema['dataModel']);
            }
            if (data && chipsOutput.length) {
                formData = typeGuard.toAscii(data['form']);
                chips = formData[chipsOutput];
            }
            if (typeGuard.checkType(schema['dataModel'], formData, 'schema')) {
                chips = this.IsJsonString(chips);
                if (chips) {
                    async.map(chips, (chip, callback) => {
                        const collection = mongoData(chip['name']);
                        collection.findOne({ _id: req.params.id }, (err, returned) => {
                            callback(err, returned);
                        });
                    }, (e, results) => {
                        const output: any = e ? e : results;
                        output.push(data);
                        res.json(output);
                    });
                }
            } else {
                res.json({ severity: 'error', summary: 'Error', detail: 'Data does not match schema' });
            }
        } catch (err) {

        }
    }

    public guardDocForm(req: Request, res: Response) {
        try {
            async.autoInject({
                typeguard(callback) {
                    typeGuard.checkType(docSchema, req.body, 'data') ? callback(null, req.body)
                        : callback('Data does not match schema', null);
                }
            }, (err, results) => {
                res.json((err) ? err : results.typeguard);
            });
        } catch (err) {
            return err;
        }
    }

    public findProp(obj, prop) {
        const that = this;
        if (obj[prop]) {
            return obj[prop];
        }
        Object.keys(obj).forEach((key) => {
            if (_.isObject(obj[key])) {
                return that.findProp(key, prop);
            }
        });
        return null;
    }

    public computeElements(id, formName, formField, formValue) { // all string
        let computation;
        try {
            if (!formValue) {
                formValue = '';
            }
            const sample = [{ id, name: 'API', field: formField, value: formValue }];
            const direct = `API.${formField}`;
            computation = {
                formula: { outputType: 'Alphanumeric', functionType: null, functionValue: null },
                [direct]: sample
            };
            return computation;
        } catch (err) {
            return computation;
        }
    }

    public async pullForeignData(req: Request, res: Response) {
        const clientChips = [
            'API_ClientForm',
            'API_addresses',
            'API_user',
            'API_teams',
            'API_userIdArray'
        ];
        try {
            switch (true) {
                case req.params.formName === 'API_search' || !req.params.formName: // no params
                    this.pullClientData(req, res);
                    break;
                case !req.params.userId: // only form param
                    this.pullClientForms(req, res);
                    break;
                case req.params.userId && !ObjectId.isValid(req.params.userId):
                    this.errorResponse(req, res);
                    break;
                case clientChips.includes(req.params.formName): // 2 params
                    this.pullClientObject(req, res);
                    break;
                case !clientChips.includes(req.params.formName): // 2 params
                    this.reformClientCache(req, res); // [{name, id}]
                    break;
                default:
                    this.pullClientData(req, res);
                    break;
            }
        } catch (err) {
            res.json({ severity: 'error', summary: 'Error', detail: err });
        }
    }

    public errorResponse(req: Request, res: Response) {
        res.json({ severity: 'error', summary: 'Error', detail: 'Id is invalid' });
    }

    public getClient(api, Schema, callback) {
        const output = [];
        const that = this;
        request.get(api, function optionalCallback(err, httpResponse, body) {
            if (err) {
                return { err };
            }
            if (body.length) {
                let limit = 0;
                const newBody = that.IsJsonString(body);
                if (newBody) {
                    for (const client of newBody) {
                        if (limit >= 25) {
                            break;
                        } else if (client._id) {
                            _.keys(client).map((res) => {
                                if (!Schema.includes(res)) {
                                    delete client[res];
                                }
                            });
                            output.push(client);
                            ++limit;
                        }
                    }
                }
            }
            callback(err, output);
        });
    }

    public async pullClientData(req: Request, res: Response) {
        try {
            const Schema = [
                '_id',
                'lastName',
                'firstName',
                'email',
                'phone',
                'ssn',
                'ssnKey',
                'username',
                'name',
                'externalIdentifier'
            ];
            const companyId = [
                '5b45ffe3759dbf1165cf6db6',
                '5b45fffa759dbf1165cf6db7',
                '5b8fdc270bce3abd0f9d279f',
                '5b8fdc3a0bce3abd0f9d27a0',
                '5b8fdc110bce3abd0f9d279e'
            ];
            const that = this;
            const searchOpts = req.params.userId || 'Dpe';
            const api = 'https://apidev.teamandtech.com/client/search?value=' + searchOpts + '&companyId=';

            async.map(companyId.map((result) => api + result), (call, callback) => {
                that.getClient(call, Schema, (err, response) => callback(err, response));
            }, (e, results) => {
                const output: any = e ? e : results;
                res.json({ form: typeGuard.toHex(_.flatten(output)) });
            });
        } catch (err) {
            res.send(200).json({ severity: 'error', summary: 'Error', detail: err });
        }
    }

    public async pullClientForms(req: Request, res: Response) {
        const MongoForm = mongoForm(baseCollection);
        try {
            async.autoInject({
                getSchema(callback) {
                    MongoForm.find({ reference: req.params.formName }, (err: any, form: any) => callback(err, form));
                },
                typeguard(getSchema, callback) {
                    const form = typeGuard.toAscii((getSchema.length) ? getSchema[getSchema.length - 1]['form'] : getSchema['form']);
                    typeGuard.checkType(formSchema, form, 'data') ? callback(null, getSchema[0])
                        : callback('Data does not match schema', null);
                }
            }, (err, results) => {
                res.json((err) ? err : results.typeguard);
            });
        } catch (err) {
            return err;
        }
    }

    public async pullClientObject(req: Request, res: Response) {
        const that = this;
        try {
            async.autoInject({
                allForms(callback) {
                    mongoForm(baseCollection).find({}, (err: any, form: any) => callback(err, form));
                },
                pullClientAnswers(callback) {
                    that.pullQuestionList(req.params.userId, (err: any, data: any) => callback(err, data));
                },
                questionList(pullClientAnswers, callback) {
                    const parsedBody = that.IsJsonString(pullClientAnswers);
                    // console.log('parsedBody', parsedBody);
                    const output = [];
                    if (parsedBody) {
                        for (const item of parsedBody) {
                            output.push(item['questionId']);
                        }
                    }
                    callback(null, output);
                },
                /// All relevant forms ///
                pullFormsIds(allForms, questionList, callback) {
                    that.pullFormsForIds(allForms, questionList, (err: any, forms: any) => callback(err, forms));
                },
                pullSchema(allForms, callback) {
                    const arrays = allForms as any;
                    const client = arrays.find((ref) => ref['reference'] === req.params.formName);
                    callback(null, (client) ? typeGuard.toAscii(client['form']) : []);
                },
                requestObject(pullFormsIds, allForms, pullSchema, callback) {
                    const arrays = allForms as any;
                    const client = arrays.find((obj) => obj['reference'] === req.params.formName);
                    let externalChips = pullFormsIds as any;
                    externalChips = externalChips.map((ext) => {
                        return { name: ext, id: req.params.userId };
                    });
                    that.requestObject(req.params.formName, req.params.userId, pullSchema, client, externalChips, (err, requested) => {
                        callback(err, requested);
                    });
                },
                buildClientPersistCache(requestObject, pullFormsIds, pullClientAnswers, callback) {
                    const clientAnswers = that.IsJsonString(pullClientAnswers) as any;
                    let externalChips = pullFormsIds as any;
                    const form = typeGuard.toAscii(requestObject);
                    const ChipsForm = chipsForm();
                    const answers = [];

                    externalChips = externalChips.map((ext) => {
                        return { name: ext, id: req.params.userId };
                    });
                    if (clientAnswers && clientAnswers.length) {
                        for (const answer of clientAnswers) {
                            answers.push({
                                _id: answer._id,
                                userId: answer.userId,
                                formId: req.params.userId,
                                questionId: answer.questionId,
                                answer: answer.answer,
                                dateCreated: answer.dateCreated || null,
                                dateUpdated: answer.dateUpdated || null,
                                lastCreatedBy: answer.lastCreatedBy || null,
                                lastUpdatedBy: answer.lastUpdatedBy || null,
                                parentResponseId: answer.parentResponseId || ''
                            });
                        }
                    }
                    ChipsForm.findOneAndUpdate({ _id: req.params.userId },
                        {
                            persist: JSON.stringify({ [req.params.formName]: form }),
                            queue: externalChips, answers: JSON.stringify(answers)
                        }, { new: true, upsert: true }, (err, doc) => {
                            callback(err, doc);
                        });
                }
            }, (err, results) => {
                res.json((err) ? err : { form: results.requestObject });
            });
        } catch (err) {
            res.json({ severity: 'error', summary: 'Error', detail: err });
        }
    }

    public async requestObject(formName, userId, schema, form, externalChips, CB) {
        const Schema = [
            '_id',
            'lastName',
            'firstName',
            'email',
            'phone',
            'ssn',
            'ssnKey',
            'username',
            'name',
            'externalIdentifier'
        ];
        const that = this;
        let item;
        let returnValue;
        const chipsOutput: any = that.getChipsList(schema.dataModel);
        const chipsArray = [];
        const output = [];
        const list = [];
        const api = 'https://apidev.teamandtech.com/client/find/' + userId;
        try {
            request.get(api, function optionalCallback(err, httpResponse, body) {
                if (!err && body.length && userId && form) {
                    item = schema.dataModel.schema;
                    let clientItem = that.IsJsonString(body) || [];
                    if (formName !== 'API_ClientForm') {
                        clientItem = that.findProp(clientItem, formName.split('_')[1]);
                    }
                    if (clientItem) {
                        returnValue = that.processClientData(clientItem, Schema, formName, userId, chipsOutput, externalChips, schema);
                    }
                    output.push(returnValue);
                }
                CB(err, typeGuard.toHex(output));
            });
        } catch (err) {
            CB(err, []);
        }
    }

    public processClientData(clientItem, Schema, formName, userId, chipsOutput, externalChips, schema) {
        let chipsArray = [];
        const list = [];
        let returnValue;
        if (schema) {
            const item = schema['dataModel']['schema'];
            if (item && _.isObject(item.properties)) {
                const temp = Object.keys(item.properties);
                _.keys(clientItem).map((res) => {
                    if (temp.includes(res)) {
                        item.properties[res] = {
                            ...item.properties[res]
                            // computation: JSON.stringify(that.computeElements(clientItem._id, formName, res, clientItem[res]))
                        };
                    }
                    if (!Schema.includes(res)) {
                        if (res[0] !== '_' && (_.isArray(clientItem[res]) || _.isObject(clientItem[res])) && chipsOutput.length) {
                            chipsArray.push({ name: 'API_' + res, id: userId });
                            for (const chip of list) {
                                chipsArray.push({ name: chip, id: userId });
                            }
                            chipsArray = [
                                ...chipsArray,
                                ...externalChips
                            ];
                        }
                        delete clientItem[res];
                    }
                });
                if (chipsOutput.length) {
                    if (formName !== 'API_ClientForm') {
                        chipsArray.push({ name: 'API_ClientForm', id: userId });
                    }
                    clientItem = {
                        ...clientItem,
                        [chipsOutput]: JSON.stringify(chipsArray)
                    };
                }
                if (item.properties[chipsOutput]) {
                    item.properties[chipsOutput]['Form'] = JSON.stringify(chipsArray);
                    item.properties[chipsOutput]['selectedOpt'] = JSON.stringify(chipsArray);
                }
                returnValue = {
                    form: schema['dataModel']['form'],
                    formData: clientItem,
                    formModel: schema['formModel'],
                    formId: schema['_id'],
                    schema: item
                };
                return returnValue;
            }
        }
    }

    public async reformClientCache(req: Request, res: Response) {
        const that = this;
        let returnValue;
        async.autoInject({
            pullQuestions(callback) {
                that.pullAllQuestions((err: any, form: any) => callback(err, form));
            },
            pullExistingPersist(callback) {
                chipsForm().findOne({ _id: req.params.userId }, (err: any, form: any) => callback(err, form));
            },
            pullForm(callback) {
                mongoForm(baseCollection).findOne({ reference: req.params.formName }, (err: any, form: any) => callback(err, form));
            },
            formQuestionList(pullExistingPersist, pullQuestions, callback) {
                if (pullQuestions && pullExistingPersist && pullExistingPersist['answers']) {
                    const questions = that.IsJsonString(pullQuestions) as any;
                    const answers = that.IsJsonString(pullExistingPersist['answers']) as any;
                    const output = [];
                    let ansObj = {};
                    if (questions && answers) {
                        for (const item of questions) {
                            ansObj = answers.find((answer) => answer.questionId === item._id);
                            if (ansObj) {
                                ansObj = {
                                    ...ansObj,
                                    ...item
                                };
                                output.push(ansObj);
                            }
                        }
                        callback(null, output);
                    } else {
                        callback(null, null);
                    }
                }
            },
            restructureFormData(pullForm, formQuestionList, callback) {
                if (pullForm) {
                    const form = typeGuard.toAscii(pullForm['form']);
                    const answers = formQuestionList as any;
                    const formData = {};
                    const persist = [];
                    if (answers && form) {
                        for (const property of form['formModel']['data']) {
                            answers.forEach((ans) => ans['name'] = that.fixName(ans.name));
                            let foundMatch = answers.find((answer) => that.fixName(answer.name) === property['ref']
                                || answer['questionId'] === property['questionId']);
                            if (foundMatch) {
                                foundMatch = that.transformElement(foundMatch);
                                formData[property['ref']] = foundMatch['answer'];
                                persist.push(foundMatch);
                            }
                        }
                    }
                    callback(null, [formData, persist]);
                } else {
                    callback(null, null);
                }
            },
            matchFormWithData(pullForm, restructureFormData, pullExistingPersist, callback) {
                if (pullForm && restructureFormData && pullExistingPersist) {
                    const form = typeGuard.toAscii(pullForm['form']);
                    const chipsList = that.getChipsList(form['dataModel']);
                    returnValue = that.processClientData(restructureFormData[0], _.keys(restructureFormData[0]),
                        req.params.formName, req.params.userId, chipsList,
                        pullExistingPersist['queue'], form);
                }
                callback(null, returnValue);
            }
        }, (err, results) => {
            res.json((err) ? err : { form: typeGuard.toHex([results.matchFormWithData]) });
        });

    }

    public pullQuestionList(clientID: any, callback) {
        const api_formData = 'https://apidev.teamandtech.com/ifs/form-data/' + clientID;
        try {
            request.get(api_formData, function optionalCallback(err, httpResponse, body) {
                callback(err, body);
            });
        } catch (err) {
            callback(err, null);
        }
    }

    public pullFormsForIds(allForms: any, questionList: any, callback) {
        const output = {};
        try {
            if (allForms.length && questionList.length) {
                const arrays = allForms as any;
                const QL = questionList as any;
                for (const item of arrays) {
                    const parsedForm = typeGuard.toAscii(item['form']);
                    const formModel = parsedForm['formModel'];
                    if (formModel) {
                        formModel.data.map((prop) => {
                            if (QL.includes(prop['questionId'])) {
                                output[formModel.formName] = prop['questionId'];
                            }
                        });
                    }
                }
            }
            callback(null, _.keys(output));
        } catch (err) {
            callback(err, []);
        }
    }

    public IsJsonString(str) {
        try {
            return JSON.parse(str);
        } catch (e) {
            return false;
        }
    }

    public pullAllQuestions(callback) {
        try {
            const questionApi = 'https://apidev.teamandtech.com/question/all';
            request.get(questionApi, function optionalCallback(err, httpResponse, body) {
                callback(err, body);
            });
        } catch (err) {
            callback(err, null);
        }
    }

    public transformElement(foundMatch) {
        const types = ['checkbox', 'radio', 'select'];
        if (types.includes(foundMatch['questionType'])) {
            const parsed = this.IsJsonString(foundMatch['answer']) as any;
            if (parsed && (foundMatch['questionType'] === 'radio' || foundMatch['questionType'] === 'select') && _.isObject(parsed)) {
                const obj = parsed.find((res) => (res.checked) || res.value === 'on');
                foundMatch['answer'] = obj ? obj.value : null;
            } else if (parsed && foundMatch['questionType'] === 'checkbox' && _.isObject(parsed)) {
                const answers = [];
                for (const item of parsed) {
                    if (item['checked'] || item['value'] === 'on') {
                        answers.push(item.value);
                    }
                }
                foundMatch['answer'] = answers;
            }
        }
        return foundMatch;
    }
    private fixName(name) {
        const newName = name.replace(/\//g, '_');
        return newName.replace(/\\/g, '_');
    }

    public checkDataIntegrity(req: Request, res: Response) {
        const that = this;
        try {
            async.autoInject({
                pullForm(callback) {
                    const MongoForm = mongoForm(baseCollection);
                    MongoForm.findOne({ reference: req.params.formName }, (err: any, form: any) => {
                        callback(err, form);
                    });
                },
                pullData(pullForm, callback) {
                    let api;
                    if (req.params.formName === 'API_ClientForm' && pullForm) {
                        api = 'https://apidev.teamandtech.com/client/';
                    } else {
                        api = 'https://tnt-mock-node-api.herokuapp.com/input-element?formId=' + pullForm['_id'];
                    }
                    request.get(api, (err, httpResponse, body) => {
                        callback(err, that.IsJsonString(body));
                    });
                },
                transformData(pullData, pullForm, callback) {
                    const cache = {};
                    const form = typeGuard.toAscii(pullForm['form']);
                    if (form && pullData && req.params.formName !== 'API_ClientForm') {
                        for (const answer of pullData as any) {
                            if (!cache[answer['clientId']]) {
                                cache[answer['clientId']] = {};
                            }

                            for (const item of form['formModel']['data']) {
                                if (item['questionId'] === answer['questionId']) {
                                    cache[answer['clientId']][item['ref']] = answer['answer'] || answer['singleAnswer'];
                                }
                            }
                        }
                        pullData = _.values(cache).map((value) => value);
                    }
                    callback(null, pullData);
                },
                typeguard(pullForm, transformData, callback) {
                    const form = typeGuard.toAscii(pullForm['form']);
                    const report = {
                        total: 0,
                        // userIdCount: 0,
                        pass: 0,
                        partial: 0,
                        fail: 0
                        // partially: [],
                        // failed: []
                    };
                    if (pullForm && transformData) {
                        for (const item of transformData as any) {
                            report.total++;
                            for (const prop of Object.keys(item)) {
                                // if (prop === 'userId' && item[prop] !== '')
                                // report.userIdCount++;
                                if (_.isArray(item[prop]) || _.isObject(item[prop]) || prop === '__v' || prop === '_id') {
                                    delete item[prop];
                                }
                            }
                            if (typeGuard.checkType(form['dataModel'], item, 'schema')) {
                                report.pass++;
                            } else if (typeGuard.checkType(form['dataModel'], item, 'filter')) {
                                report.partial++;
                                // report.partially.push(item);
                            } else {
                                report.fail++;
                                // report.failed.push(item);
                            }
                        }
                        callback(null, report);
                    } else {
                        callback({ severity: 'error', summary: 'Error', detail: 'Form/Data not found' }, null);
                    }
                }
            }, (err, results) => {
                res.json((err) ? err : results.typeguard);
            });
        } catch (err) {
            return err;
        }
    }

    public async cleanData(req: Request, res: Response) {
        const that = this;
        try {
            const MongoForm = mongoForm(baseCollection);
            // let quasi = await MongoForm.findOne({ reference: 'Quasi_Form'}); // full function will get every form
            // let quasiForm = typeGuard.toAscii(quasi['form']);
            const allForms = await MongoForm.find({});
            const questionCache = {};
            for (const form of allForms) {
                const tempForm = typeGuard.toAscii(form['form']);
                for (const question of tempForm['formModel']['data']) {
                    if (!questionCache[question['ref']]) {
                        questionCache[question['ref']] = { id: '', forms: [] };
                    }
                    questionCache[question['ref']]['id'] = question['questionId'];
                    questionCache[question['ref']]['forms'].push(form['_id']);
                    console.log(questionCache);
                }
            }

            // request.get('https://apidev.teamandtech.com/client', function optionalCallback(err, httpResponse, body) {
            //     let response = that.IsJsonString(body), questionCache = {}, output = [], count = 0;

            // for (let client of response) {
            //     for (let property of Object.keys(client)) {
            //         if (property[0] !== '_' && questionCache[property]) {
            //             if (_.isObject(client[property])){
            //                 for (let prop of Object.keys(client[property])){
            //                     output.push(that.createClientAnswer(prop, client[property], questionCache, formsCache[form]['_id']));
            //                 }
            //             }
            //             else if (_.isArray(client[property]) && property !== 'userIdArray'){
            //                 for (let index of client[property]){
            //                     output.push(that.createClientAnswer(property, client[index], questionCache, formsCache[form]['_id']));
            //                 }
            //             }
            //             else{
            //                 output.push(that.createClientAnswer(property, client, questionCache, formsCache[form]['_id']));
            //             }
            //             count++;
            //         }
            //     }
            //     // if (output.length >= 10) {
            //     //     request.post('https://tnt-mock-node-api.herokuapp.com/input-element/batch',
            //  { json: output }, function optionalCallback(err, httpResponse, body) {
            //     //         console.log('response: ', httpResponse.statusCode);
            //     //     });
            //     //     output = [];
            //     // }
            // }
            // }
            // request.post('https://tnt-mock-node-api.herokuapp.com/input-element/batch',
            // { json: output }, function optionalCallback(err, httpResponse, body) {
            //     console.log(httpResponse.statusCode);
            // });
            res.json(questionCache);
            // });
        } catch (e) {
            res.json('error: ' + e);
        }
    }

    public createClientAnswer(property, client, questionCache, formId) {
        const userIdArray = (client['userIdArray'] && client['userIdArray'][0]) ? client['userIdArray'][0] : null;
        const newObjId = (client['userId'] || userIdArray) ? client['userId'] || userIdArray : new ObjectId();
        const output = {
            userId: newObjId.toString(),
            formId,
            clientId: client['_id'] || newObjId,
            questionId: questionCache[property],
            answer: client[property] || '',
            questionName: property,
            singleAnswer: client[property] || '',
            dateCreated: client['dateCreated'] || +new Date(),
            dateUpdated: +new Date(),
            lastCreatedBy: client['lastCreatedBy'] || 'API-createClientAnswer',
            lastUpdatedBy: 'API-createClientAnswer',
            parentResponseId: null
        };
        return output;
    }
}
