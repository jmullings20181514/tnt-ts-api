import { Request, Response } from 'express';
import * as _ from 'lodash';
import * as mongoose from 'mongoose';
import * as TypeGuard from 'ts-runtime-typeguard';
import { PersistSchema } from '../models/model';
import { Utility } from './utility.service';

export const typeGuard = new TypeGuard();
export const utility = new Utility();
const PersistForm = mongoose.model('persistforms', PersistSchema);

export class PersistService {

    public async getPersist(req: Request, res: Response) {
        try {
            const persist = await PersistForm.findOne({ reference: req.params.persistId });
            res.json(persist ? persist :
                { severity: 'error', summary: 'Error', detail: 'Persist does not exist' });
        } catch (err) {
            res.json({ severity: 'error', summary: 'Error', detail: err });
        }
    }

    public async getAllPersist(req: Request, res: Response) {
        try {
            const form = await PersistForm.find({});
            res.json(form && form.length ? form :
                { severity: 'error', summary: 'Error', detail: 'Persist collection empty' });
        } catch (err) {
            res.json({ severity: 'error', summary: 'Error', detail: err });
        }
    }

    public async upsertPersist(req: Request, res: Response) {
        try {
            const persist = await PersistForm.findOneAndUpdate({ reference: req.params.persistId },
                req.body, { new: true, upsert: true });
            res.json(persist ? persist :
                { severity: 'error', summary: 'Error', detail: 'Persist collection unavailable' });
        } catch (err) {
            res.json({ severity: 'error', summary: 'Error', detail: err });
        }
    }

    public async deletePersist(req: Request, res: Response) {
        try {
            const form = await PersistForm.findOneAndDelete({ reference: req.params.persistId });
            res.json(form ? form :
                { severity: 'error', summary: 'Error', detail: 'Persist does not exist' });
        } catch (err) {
            res.json({ severity: 'error', summary: 'Error', detail: err });
        }
    }
}
