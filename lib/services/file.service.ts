import * as docxtemplater from 'docxtemplater';
import * as fs from 'fs';
import * as jszip from 'jszip';
import * as pdfkit from 'pdfkit';

export default {
  generatePDF(data) {
    return new Promise((resolve) => {
      const pdf = new pdfkit();
      const buffers = [];
      pdf.on('data', (content) => buffers.push(Buffer.from(content)));
      pdf.on('end', () => resolve(Buffer.concat(buffers)));
      pdf.text(data);
      pdf.end();
    });
  },

  generateDocx(data, template = '/../../dist/templates/test-template.docx') {
    return new Promise((resolve) => {
      const content = fs.readFileSync(__dirname + template, 'binary');
      const zip = new jszip(content);
      const doc = new docxtemplater();
      doc.loadZip(zip);
      doc.setData(data);
      doc.render();
      const buffer = doc.getZip().generate({ type: 'nodebuffer' });
      resolve(buffer);
    });
  }
};
