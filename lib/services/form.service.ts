import { Request, Response } from 'express';
import * as _ from 'lodash';
import * as mongoose from 'mongoose';
import * as request from 'request';
import * as TypeGuard from 'ts-runtime-typeguard';
import { FormSchema } from '../models/model';
import { Utility } from './utility.service';

export const typeGuard = new TypeGuard();
export const utility = new Utility();
export const formSchema = {
    formModel: {
        formName: 'formName',
        data: []
    },
    dataModel: {
        form: [],
        schema: {}
    }
};
const MongoForm = mongoose.model('generalforms', FormSchema);

export class FormService {

    public async getOneForm(req: Request, res: Response) {
        try {
            const form = await MongoForm.findOne({ reference: req.params.formId });
            res.json(form ? form :
                { severity: 'error', summary: 'Error', detail: 'Form does not exist' });
        } catch (err) {
            res.json({ severity: 'error', summary: 'Error', detail: err });
        }
    }

    public async getAllForms(req: Request, res: Response) {
        try {
            const form = await MongoForm.find({});
            form.forEach((hexed) => hexed['form'] = '');
            res.json(form && form.length ? form :
                { severity: 'error', summary: 'Error', detail: 'Forms collection empty' });
        } catch (err) {
            res.json({ severity: 'error', summary: 'Error', detail: err });
        }
    }

    public async addFormByName(req: Request, res: Response) {
        try {
            if (typeGuard.checkType(formSchema, typeGuard.toAscii(req.body.form), 'data')) {
                const checkForm = await this.checkFormExternal(req.body.reference);
                const foreignRef = utility.IsJsonString(checkForm as any) ? utility.IsJsonString(checkForm as any)
                    : await this.addFormExternal(req.body.reference);
                if (foreignRef && foreignRef._id) {
                    const formId = foreignRef._id;
                    const unpackedForm = typeGuard.toAscii(req.body.form);
                    unpackedForm['_id'] = formId;
                    req.body = {
                        ...req.body,
                        form: typeGuard.toHex(unpackedForm),
                        _predRef: await utility.getChainRef(req.body, MongoForm)
                    };
                    MongoForm.findOneAndUpdate({ _id: formId }, req.body,
                        { new: true, upsert: true, setDefaultsOnInsert: true }
                        , (err, newForm) => {
                            res.json(err ?
                                { severity: 'error', summary: 'Error', detail: err || 'Cannot add form' } : newForm);
                        });
                } else {
                    res.json({ severity: 'error', summary: 'Error', detail: 'Foreign API is unavailable' });
                }
            }
        } catch (err) {
            res.json({ severity: 'error', summary: 'Error', detail: err });
        }
    }

    public async checkFormExternal(reference) {
        const _checkFormExternal = new Promise((resolve, reject) => {
            request.get('https://apidev.teamandtech.com/ifs/form?name=' + reference, (err, httpResponse, body) => {
                err ? reject(err) : resolve(body);
            });
        });
        const response = await _checkFormExternal;
        return response;
    }

    public async addFormExternal(reference) {
        const _addFormExternal = new Promise((resolve, reject) => {
            request.post('https://apidev.teamandtech.com/form/add',
                { json: { name: reference, externalKey: null } }, (err, httpResponse, body) => {
                    err ? reject(err) : resolve(body);
                });
        });
        const response = await _addFormExternal;
        return response;
    }
}
