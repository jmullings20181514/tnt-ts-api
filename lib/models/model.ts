import * as mongoose from 'mongoose';

const Schema = mongoose.Schema;

export const PersistSchema = new Schema({
    reference: String,
    persist: String
});

export const ChipsSchema = new Schema({
    user: String,
    persist: String,
    queue: Array,
    answers: String
});

const FormStructure = {
    form: String,
    reference: String,
    _predRef: String || null,
    clientName: String || null
};

export const InputElementSchema = new Schema({
    formId: { type: String, required: true },
    clientId: { type: String, required: true },
    questionId: { type: String, required: true },
    userId: { type: String, required: true },
    answer: String,
    singleAnswer: String,
    dateCreated: Number,
    dateUpdated: Number,
    lastCreatedBy: String,
    lastUpdatedBy: String,
    parentResponseId: String
});

export const FormSchema = new Schema(FormStructure);

export const FormData = new Schema(FormStructure);
