const request = require('supertest');
const app = require('../../dist/app').default;

/**
 * Testing get all forms endpoint
 */
describe('GET /', function () {
    it('respond with simple message', function (done) {
        request(app)
            .get('/')
            .set('Accept', 'application/json')
            .expect('Content-Type', /json/)
            .expect(200, done);
    });
});

/**
 * Testing get all forms endpoint
 */
describe('GET /form', function () {
    it('respond with json containing a list of all forms', function (done) {
        request(app)
            .get('/form')
            .set('Accept', 'application/json')
            .expect('Content-Type', /json/)
            .expect(200, done);
    });
});

/**
 * Testing get all persist endpoint
 */
describe('GET /persist', function () {
    it('respond with json containing a list of all persisted data', function (done) {
        request(app)
            .get('/form')
            .set('Accept', 'application/json')
            .expect('Content-Type', /json/)
            .expect(200, done);
    });
});

/**
 * Testing get all data elements endpoint
 */
describe('GET /elements', function () {
    it('respond with json containing a list of all data elements', function (done) {
        request(app)
            .get('/elements')
            .set('Accept', 'application/json')
            .expect('Content-Type', /json/)
            .expect(200, done);
    });
});

describe('POST /persist/:persistId', function () {
    it('respond with json containing a list of all data elements', function (done) {
        request(app)
            .post('/persist/0x')
            .send({
                reference: '0x'
            })
            .set('Accept', 'application/json')
            .expect('Content-Type', /json/)
            .expect(200, done);
    });
});