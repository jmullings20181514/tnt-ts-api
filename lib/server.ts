import * as fs from 'fs';
import * as https from 'https';
import app from './app';

const httpsOptions = {
    key: fs.readFileSync('./config/key.pem'),
    cert: fs.readFileSync('./config/cert.pem')
};

// https.createServer(httpsOptions, app).listen(process.env.PORT || 8088, () => {
//     console.log('Express server listening on port ' + process.env.PORT || 8088);
// });

app.listen(process.env.PORT || 8088, () =>
    console.log('Express server listening on port ' + process.env.PORT || 8088)
);
